"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import Dropdown from "./components/dropdown.js";
import Spoilers from "./components/spoilers.js";
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import {WebpMachine} from "webp-hero"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();


// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();


// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});


document.addEventListener('scroll', (event) => {
    const header = document.querySelector('[data-header]')
    const sticky = document.querySelector('.header-sticky')
    const buttons = document.querySelector('.primary__buttons')

    if (sticky.getBoundingClientRect().top < 0) {
        header.classList.add("header--fixed");
    } else {
        header.classList.remove("header--fixed");
    }


    if (buttons.getBoundingClientRect().top < 0) {
        header.classList.add("header--buttons");
    } else {
        header.classList.remove("header--buttons");
    }
})

// Counters
let Counters = () => {
    const counters = document.querySelectorAll('[data-counter]')
    if (counters.length > 0) {
        counters.forEach(elem => {
            let counterNum =  parseInt(elem.dataset.counter)
            let counterStep =  parseInt(elem.dataset.step)
            let counterTime =  parseInt(elem.dataset.time)

            let n = 0;
            let t = Math.round(counterTime / (counterNum / counterStep));


            let interval = setInterval(() => {
                n = n + counterStep;
                if (n === counterNum) {
                    clearInterval(interval);
                }
                elem.innerHTML = usefulFunctions.getDigFormat(n);

            }, t);
        });
    }
}

window.addEventListener("load", function (e) {

    if(document.querySelector('[data-counters]')) {
        Counters()
    }
});
